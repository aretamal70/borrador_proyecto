const fs = require('fs');

//Escribe a fichero el dato
function writeUserDataToFile(data) {
	var jsonUserData = JSON.stringify(data);

	fs.writeFile("./usuarios.json", jsonUserData, "utf8",
			function(err) {
				if (err){
					console.log (err);
				} else 	{
					console.log ("Datos de Usuario escritos");
				}
			}
	)
}

module.exports.writeUserDataToFile = writeUserDataToFile; //Necesario para poder usarlo fuera de este modulo
