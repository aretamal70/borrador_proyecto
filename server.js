require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//Para habilitar la conexion entre dominios
var enableCORS = function(req, res, next){
		res.set("Access-Control-Allow-Origin", "*");
		res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
		res.set("Access-Control-Allow-Headers", "Content-Type");
		next();
}

app.use(express.json());
app.use(enableCORS);

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/logout/:id', authController.logoutV1);
app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

app.get('/apitechu/accounts', accountController.getAccounts);
app.get('/apitechu/accounts/:userId', accountController.getAccountsByUserId);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

//GET (Read)
app.get ('/apitechu/v1/hello',
		function(req, res) {
			console.log("GET /apitechu/v1/hello");
			res.send({"msg" : "Hola desde API TechU"});
		}
);

//POST (Create con headers)
app.post('/apitechu/v1/monstruo/:p1/:p2',
		function (req, res) {
			  console.log("Parámetros");
				console.log(req.params); // Parámetros
				console.log("Headers");
				console.log(req.headers); // Headers
				console.log("Body");
				console.log(req.body); // Body
				console.log("Query string");
				console.log(req.query);	// Query string
		}
);
