const bcrypt = require('bcrypt');

function hash(data){
    console.log("Hashing data");
    return bcrypt.hashSync(data, 10);
}

function checkPass(passPlainText, passDBHashed){
  console.log("Comprobando password");
  return bcrypt.compareSync(passPlainText, passDBHashed);
}

module.exports.hash = hash;
module.exports.checkPass = checkPass;
