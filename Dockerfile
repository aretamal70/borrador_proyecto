#Imagen raiz
FROM node

#Carpeta raiz
WORKDIR /apitechu

#Copia de archivos de carpeta local a apitechu
ADD . /apitechu

#Instalación de dependencias
RUN npm install

#Puerto que expone
EXPOSE 3000

#Comando de inicialización
CMD ["npm", "start"]

