const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersByIdV2(req, res) {
  console.log('GET /apitechu/v2/users/:id');
  //console.log("id introducido " + req.params.id);
  var id = req.params.id;

  var query = 'q={"id":' + id + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
          //var response = !err ? body : {
          //    "msg" : "Error obteniendo usuarios"
          //}

          //si llega un error del servidor remoto ->
          if (err) {
            var response = { "msg" : "Error obteniendo usuario" };
            res.status(500);
          } else {
              if (body.length > 0) {
                var response = body[0];
              } else {
                var response = { "msg" : "Usuario no encontrado" };
                res.status(404);
              }
          }

          //console.log(response);
          res.send(response);
    }
  );
}

function getUsersV2(req, res) {
  console.log('GET /apitechu/v2/users');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("users?" + mLabAPIKey,
    function(err, resMlab, body){
          var response = !err ? body : {
              "msg" : "Error obteniendo usuarios"
          }

          res.send(response);
    }
  );
}

function getUsersV1(req, res) {
  console.log('GET /apitechu/v1/users');

  var users = require('../usuarios.json');
  var result = {};

  if (req.query.$count == "true") {
    result.count = users.length;
  }

  result.users = req.query.$top ? users.slice (0, req.query.$top) : users;
  res.send(result);
}

//Introduce nuevo user
function createUserV1(req, res) {
      console.log('POST /apitechu/v1/users');
      //console.log(req.Body);
      var newUser = {
          "first_name": req.body.first_name,
          "last_name":  req.body.last_name,
          "email":      req.body.email
      }

      var users = require('../usuarios.json');
      users.push(newUser);
      console.log("usuario añadido");
      io.writeUserDataToFile(users);
}

//Introduce nuevo user
function createUserV2(req, res) {
      console.log('POST /apitechu/v2/users');
      //console.log(req.Body);
      var newUser = {
          "id":         req.body.id,
          "first_name": req.body.first_name,
          "last_name":  req.body.last_name,
          "email":      req.body.email,
          "password":   crypt.hash(req.body.password)
      };

      var httpClient = requestJson.createClient(baseMlabURL);
      console.log("cliente creado");

      httpClient.post("users?" + mLabAPIKey, newUser,
        function(err, resMlab, body){
              console.log("Usuario creado correctamente");
              res.status(201).send({"msg": "Usuario creado correctamente"});
        }
      );
      //var users = require('../usuarios.json');
      //users.push(newUser);
      console.log("usuario añadido");
      //io.writeUserDataToFile(users);
}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  //console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleted = false;

  // console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
     function(element){
       //console.log("comparando " + element.id + " y " +   req.params.id);
      return element.id == req.params.id
    }
  )

  //console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    users.splice(indexOfElement, 1);
    deleted = true;
  }

  if (deleted) {
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado.";

  console.log(msg);
  res.send({"msg" : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
