const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV2(req, res) {
  console.log('POST /apitechu/v2/loginV2');

  var response = {};
  response.msg = "";

  var userLogado = {
      "email": req.body.email,
      "password":  req.body.password
  }

  var query = 'q={"email": "' + userLogado.email + '"}';
  //console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  //console.log("cliente creado");

  httpClient.get("users?"
    + query
    + "&"
    + mLabAPIKey,
    function(err, resMlab, body){
          //si llega un error del servidor remoto ->
          if (err) {
             response = { "msg" : "Error obteniendo usuario" };
             res.status(500);
          } else {
               if (body.length > 0) {
                 response = body[0];
               } else {
                 var response = { "msg" : "Usuario no encontrado" };
                 res.status(404);
               }
          }

          //console.log("Pass body: " + userLogado.password + " Pass Mlab: " + response.password);

          var loginOK = crypt.checkPass(userLogado.password, response.password);

          response.msg = loginOK ?
          "Login correcto" : "Login incorrecto";

          console.log("Login msg: " + response.msg);
          if (loginOK) {
              var putBody = '{"$set":{"logged":true}}';

              httpClient.put("users?"
              + query
              + "&"
              + mLabAPIKey,
              JSON.parse(putBody),
                function(errPUT, resMlabPUT, bodyPUT) {
                  if (errPUT) {
                     response = { "msg" : "Error actualizando logged de usuario" };
                     res.status(500);
                  } else {
                       if (body.length > 0) {
                         response = body[0];
                       } else {
                         var response = { "msg" : "Usuario no encontrado" };
                         res.status(404);
                       }
                  }
                }
             );
          }
          res.send(response);
    }
  );
}

function loginV1(req, res) {
  console.log('POST /apitechu/v1/loginV1');

  var salida = {};
  var users = require('../usuarios.json');

  var userLogado = {
      "email": req.body.email,
      "password":  req.body.password
  }

  var indexOfElement = users.findIndex(
     function(element){
      //console.log("comparando " + element.email + "-" + element.password + " y " + userLogado.email + "-" + userLogado.password);
      return (element.email == userLogado.email &&
        element.password == userLogado.password)
    }
  )

  var loginOK = (indexOfElement >= 0);

  salida.msg = loginOK ?
  "Login correcto" : "Login incorrecto";

  if (loginOK)
  {
    salida.id = users[indexOfElement].id;
    users[indexOfElement].logged = true;
    io.writeUserDataToFile(users);
  }

  //console.log(salida);
  res.send(salida);
}

function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logoutV2/:id");
  console.log("id es " + req.params.id);

  var response = {};

  var id = req.params.id;
  var query = 'q={"id": ' + id + '}';
  console.log("Query: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  //console.log("cliente creado");

  httpClient.get("users?"
      + query
      + "&"
      + mLabAPIKey,
      function(err, resMlab, body){
        if (err) {
           response = { "msg" : "Error obteniendo usuario" };
           res.status(500);
        } else {
             if (body.length > 0) {
               var element = body[0];
               console.log("Mlab id:" + element.id + " y req.id: " + id + " y logged: " + element.logged);

               var logoutOK = element.id == id && element.logged;

               response.msg = logoutOK ?
               "Logout correcto"  : "Logout incorrecto";

               if (logoutOK) {
                 var putBody = '{"$unset":{"logged":""}}';
                 response.id = element.id;
                 httpClient.put("users?"
                 + query
                 + "&"
                 + mLabAPIKey,
                 JSON.parse(putBody),
                   function(errPUT, resMlabPUT, bodyPUT) {
                     if (errPUT) {
                        response = { "msg" : "Error actualizando logged de usuario" };
                        res.status(500);
                     } else {
                          if (body.length <= 0) {
                            response = { "msg" : "Usuario no encontrado" };
                            res.status(404);
                          }
                     }
                   }
                 );
               }
             } else {
               response = { "msg" : "Usuario no encontrado" };
               res.status(404);
             }
        }
        console.log("salida: " + response);
        res.send(response);
      }
  );
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logoutV1/:id");
  console.log("id es " + req.params.id);

  var salida = {};
  var users = require('../usuarios.json');

  var indexOfElement = users.findIndex(
     function(element){
      //console.log("comparando " + element.id + " y " + req.params.id + " y " + element.logged);
      return (element.id == req.params.id && element.logged)
    }
  )

  var logoutOK = (indexOfElement >= 0);

  salida.msg = logoutOK ?
  "Logout correcto"  : "Logout incorrecto";

  if (logoutOK)
  {
    salida.id = users[indexOfElement].id;
    delete users[indexOfElement].logged;
    io.writeUserDataToFile(users);
  }

  //console.log(salida);
  res.send(salida);
}

//Version login Carlos
function loginV1Profe(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

//Version logout Carlos
function logoutV1Profe(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
