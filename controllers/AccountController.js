const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsByUserId(req, res) {
  console.log('GET /apitechu/accounts/:userId');
  console.log("user introducido " + req.params.id);
  var userId = req.params.userId;

  var query = 'q={"userid":' + userId + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
          if (err) {
            var response = { "msg" : "Error obteniendo cuentas de usuario" };
            res.status(500);
          } else {
              if (body.length > 0) {
                var response = body;
              } else {
                var response = { "msg" : "Id usuario no encontrado" };
                res.status(404);
              }
          }

          console.log(response);
          res.send(response);
    }
  );
}

function getAccounts(req, res) {
  console.log('GET /apitechu/accounts');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("accounts?" + mLabAPIKey,
    function(err, resMlab, body){
          var response = !err ? body : {
              "msg" : "Error obteniendo cuentas"
          }

          res.send(response);
    }
  );
}

module.exports.getAccounts = getAccounts;
module.exports.getAccountsByUserId = getAccountsByUserId;
